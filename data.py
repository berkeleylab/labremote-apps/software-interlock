#!/usr/bin/env python

#Produce safe, warning, and unsafe training data to influxdb for interlock training.
#Author: Taurean Zhang.

#******************************************
#import stuff
import datetime
import os
import random
import sys
import time

from influxdb import InfluxDBClient

#******************************************


#******************************************
# Function: generateData
# Purpose: Create data that will trigger Grafana to issue warnings but not trigger the interlock
# Variables:
#    ntcToggle - toggles warning, safe, or unsafe data for ntc (0 for safe, 1 for warning, 2 for unsafe)
#******************************************

def generateData(ntcToggle):

   # Timestamp (UTC)
   dt_now = datetime.datetime.now(datetime.timezone.utc)
   times = dt_now.strftime("%Y-%m-%dT%H:%M:%SZ")

   # Randomly generated data for influxdb to process
   if ntcToggle == 0:
       ntc = random.uniform(0,50) # Celsius
   elif ntcToggle == 1:
       ntc = random.uniform(50,70) # Celsius
   else:
       ntc = random.uniform(70,100) # Celsius
   voltage = readSerial()
   json_body = [
       {
           "measurement": "interlock_prototype",
           "tags": {
               "location": "room"
           },
           "time": times,
           "fields": {
               "T_NTC": float(ntc),
               "PS_Voltage": float(voltage)
           }
       }
   ]
   #print(ntc)
   #print(voltage)
   #print()
   return json_body



def readSerial():
    """Read the serial output from the power supply and print it to screen."""

    #serial setup
    os.chdir("/home/taurean/labRemote/")
    #runCmd = os.popen("./build/bin/powersupply -e src/configs/6-5-23-test.json -n HMP4040-inkfish -c 2 get-voltage")
    #return runCmd.read()
    return 1
def runMeasurements(client):
    try:
        for _i in range(0,120):
            line = generateData(0)
            client.write_points(line)
            time.sleep(5)
        for _j in range(0,60):
            line = generateData(1)
            client.write_points(line)
            time.sleep(5)
        for _k in range(0,120):
            line = generateData(0)
            client.write_points(line)
            time.sleep(5)
        for _l in range(0,30):
            line = generateData(2)
            client.write_points(line)
            time.sleep(5)
        client.close()
    except KeyboardInterrupt:
        client.close()
        #print()
        sys.exit(0)




#******************************************
if __name__ == "__main__":

    #------------------------------------------
    # define influxdb client
    client = InfluxDBClient('128.3.50.38', '8086', 'pixel_module_qc_rw', 'gLY341kD6Yp0', 'pixel_module_qc')
    #read serial
    runMeasurements(client)
    #print()
