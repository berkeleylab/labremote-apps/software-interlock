#!/usr/bin/env python

#Prototype Software Interlock

#******************************************
#import all the things
import argparse
import datetime
import fcntl
import json
import logging
import os
import signal
import smtplib
import socket
import sys
import time
from email.mime.text import MIMEText
from pathlib import Path

import labRemote
from influxdb import InfluxDBClient
from pixmodqc.heartbeat import HeartBeat

#******************************************

# Set up a unified logger
logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%a %b %d %X %Z %Y')
logger = logging.getLogger(__name__)

# Class to handle calculated quantities, from the data
# of one or more sensors. Includes high/low thresholds.
class Quantity:
    def __init__(self, cfg, data):
        """Defines calculated quantities and cutoffs"""
        self.name = data.get("name") # Quantity name
        self.quantType = data.get("type") # Quantity type for shutdown procedure
        self.upperWarn = float(data.get("upper-warning")) # Upper warning boundary
        self.lowerWarn = float(data.get("lower-warning")) # Lower warning boundary
        self.upperLock = float(data.get("upper-cutoff")) # Upper lockdown boundary
        self.lowerLock = float(data.get("lower-cutoff")) # Lower lockdown boundary
        self.upperEnabled = data.get("upper-enabled") # Indicates whether or not to enabled upper boundaries
        self.lowerEnabled = data.get("lower-enabled") # Indicates whether or not to enabled lower boundaries
        self.n_of_retry_limit = float(data.get("n-of-retry-influxDB-query")) # Number of retries allowed for querying data from influxDB
        self.measTime = float(data.get("measure-time")) # Maximum time since last measurement
        self.lastMeas = 0 # latest measurement
        self.status = 1 # Status of quantity. Options are: 1=GOOD, 2=WARNING, 3=CRITICAL
        self.sensors = [] # List of sensors needed to calculate this quantity
        self.client = cfg.loginInflux()

        for s, f in zip(data.get("sensors"), data.get("fields")):
            self.sensors += [Sensor(s, f, cfg)]

    def checkMeasurement(self, numOfMeas):
        """
        Checks if quantity is within boundaries, updates status accordingly
        """
        total = 0
        success = False #Flag to indicate querying success
        #Make the influxdb queries
        results = []
        for sens in self.sensors:
            logger.debug(f"Making query for {self.name}, by sensor {sens}.")
            query = sens.makeQuery(numOfMeas)
            logger.debug(query)
            result = self.client.query(query)
            logger.debug(result)

            # Retry in the case that query fails (up to 2 times after initial try)
            if len(result)==0:
                ntry=0
                logger.warning(f"Failed database query for {self.name}. Attempts = {ntry}. Limit = {self.n_of_retry_limit}.")

                while (ntry < self.n_of_retry_limit):
                    query = sens.makeQuery(numOfMeas)
                    logger.debug(query)
                    result = self.client.query(query)
                    logger.debug(result)
                    if len(result)==0:
                        ntry += 1
                        logger.warning(f"Failed database query for {self.name}. Attempts = {ntry}. Limit = {self.n_of_retry_limit}.")
                    else:
                        success = True
                        break
                if not success:
                    logger.warning(f"No data queried for {self.name}")
                    return -1
                logger.info(f"Successfully queried data for {self.name} after {ntry} retries.")

            values = [float(record.get(sens.queryMeasKey)) for record in result.get_points()]
            timeSinceMeas = self.checkTime(result)
            if (timeSinceMeas)>self.measTime:
                logger.warning(f"Time since last measurement for {self.name}: {timeSinceMeas}")
                return -1
            results += [values]

        # TODO add case where we measure 2 sensors for 1 quantity
        if (self.quantType=="dew-chuck"):
            if (len(results)!=2):
                logger.error(f"Expected results from 2 sensors for {self.name}, but read {len(results)} - please check!")
                return -1

            if (len(results[0])!=len(results[1])):
                logger.warning(f"Length of data queried for dewpoint ({len(results[0])}) does not match length of data queried for temperature ({len(results[1])}) - please check.")
            total_TmD = 0.0
            nMeas = 0
            for d, t in zip(results[0], results[1]):
                TmD = t-d
                total_TmD += TmD
                nMeas += 1

            self.lastMeas = total_TmD/nMeas

        else:
            if len(results)!=1:
                logger.error(f"Expected results from 1 sensors for {self.name}, but read {len(results)} - please check!")
                return -1

            total = 0.0
            for r in results[0]:
                total += r

            self.lastMeas = total/numOfMeas

        #self.client.close() # Do we need to close it?
        return self.checkStatus()

    def checkStatus(self):
        #logger.info("Current Average: " + str(dataResult))
        if ((self.upperEnabled and self.lastMeas > self.upperLock) or (self.lowerEnabled and self.lastMeas < self.lowerLock)):
            return 3 # Critical state, activate lockdown
        if ((self.upperEnabled and self.lastMeas > self.upperWarn) or (self.lowerEnabled and self.lastMeas < self.lowerWarn)):
            return 2 # Warning state, DOESN'T activate lockdown
        return 1

    def checkTime(self, measList):
        """
        Returns time since last measurement
        """
        currentTime = time.time()
        times = [time for time in measList.get_points()]
        lastTime = times[0]["time"]
        return currentTime-self.decodeTime(lastTime)

    def decodeTime(self, timeStr):
        """
        Converts timestamp to unix format
        """
        timestamp_format = "%Y-%m-%dT%H:%M:%S.%fZ"
        dt_object = datetime.datetime.strptime(timeStr, timestamp_format)
        return (dt_object - datetime.datetime(1970, 1, 1)).total_seconds()


# Class to handle sensors, including information on how to
# query data from that sensor from influxDB
class Sensor:
    def __init__(self, name, field, cfg):
        """Defines sensor with the measurement details and cutoffs"""
        #Name of sensor in labRemote cfg
        self.name = name
        #Comment to indicate what sensor's purpose is
        self.comment = ""
        self.queryMeasKey = field # Like T or H
        self.queryQuantity = cfg.getQuantity(name)
        self.querySetup = cfg.getSetup()
        self.queryMeasName = cfg.getMeasName()

    def makeQuery(self, numOfMeas):
        """Creates the queries to grab data from influx"""
        #Grab all necessary parameters
        query = ('SELECT '+ self.queryMeasKey + ' FROM "' + self.queryMeasName
            + '" WHERE ("Setup" = \'' + self.querySetup + '\' AND "Quantity" = \''
            + self.queryQuantity + '\') ORDER BY time DESC LIMIT ' + str(numOfMeas))
        return query

# Class to software-interlock configuration
class SoftwareInterlock:
    """
    Parses cfg for options, influx db settings, values
    for the measurement columns in the table and sets up
    the flock on the interlock file specified in lab remote
    NOTE: This only works for one interlock file definition in
    this implementation, if you want to make it work with multiple,
    recoding is necessary
    """
    def __init__(self, file):
        try:
            data = Path(file).open()
        except OSError:
            logger.error("Cfg file doesn't exist, please try again")
            sys.exit(-1)
        config = json.load(data)
        #Parses each of the individual blocks within the cfg file
        self.setup = config["setup-details"]
        self.quantityConfig = config["quantities"]
        self.notification = config["notification-settings"]
        self.shutdownSettings = config["shutdown-settings"]
        self.quantities = [] # List of quantity objects
        self.status = 1 # Indicates status of interlock. Options are: 1=GOOD, 2=WARNING, 3=CRITICAL (matches the quantity codes)
        self.moduleLV = []
        self.moduleHV = []
        self.chiller = None
        self.peltierPS = []
        self.moduleNTCs = []
        self.labRemoteCfg = json.load(Path(self.setup["lab-remote-cfg-file"]).open())
        self.measurementName = self.labRemoteCfg["monitoring"]["measurementName"]
        self.setupname = self.labRemoteCfg["setupname"]
        self.interlockFile = self.getInterlockFile()
        data.close()

        # Initialize heartbeat
        self.heartbeat = HeartBeat("software-interlock", self.setupname)

        try:
            self.heartbeat.acquireLock(errorMsg="Another instance of the software interlock is already running.")
        except Exception as e:
            logger.error(e)
            exit(1)  # Exit if another instance is running

    def makeLabRemoteCfg(self):
        #TODO: Define better name for the duplicate lab remote cfg file
        labRemoteCfg = Path(self.setup["lab-remote-cfg-file"]).open()
        fileDupe = "dupe.cfg"
        os.system("touch dupe.cfg")
        dupelabRemoteCfg = Path(fileDupe).open("w")
        for line in labRemoteCfg:
            if "interlock-file" not in line:
                dupelabRemoteCfg.write(line)
        labRemoteCfg.close()
        dupelabRemoteCfg.close()
        return fileDupe

    # Function to set up interlock files
    def getInterlockFile(self):
        deviceList = self.labRemoteCfg["devices"]
        for device in deviceList:
            #Checks to see if parameter has been defined
            communication = device["communication"]
            if "interlock-file" in communication:
                if (Path(communication["interlock-file"]).is_absolute()):
                    os.system("touch " + communication["interlock-file"])
                    #Locks interlock file and saves it within configuration
                    interlockFile = os.open(communication["interlock-file"], os.O_RDWR) # Not working with pathlib yet
                    fcntl.flock(interlockFile, fcntl.LOCK_EX)
                    #return communication["interlock-file"]
                    return interlockFile

                #Error occurs if file path is improperly defined
                logger.error("Error, file path entered isn't an absolute file path")
                logger.error("Offending path: " + device["interlock-file"])
                sys.exit(-1)

        return None

    def loginInflux(self):
        """Creates client to login to influx db with given credentials"""
        credentials = self.labRemoteCfg["datasinks"][1]
        client = InfluxDBClient(credentials["host"], credentials["port"],
            credentials["username"], credentials["password"], credentials["database"])
        return client

    def getQuantity(self, name):
        """
        Grabs the data quantity in the lab remote cfg that is used
        to identify the measurement in influxdb
        (this is a setup specific function)
        """
        deviceList = self.labRemoteCfg["monitorsensors"]
        for device in deviceList:
            if device["name"] == name:
                return device["quantity"]
        return False

    def getSetup(self):
        return self.setup["setup-monitoring"]

    def getMeasName(self):
        """
        Grabs the measurement name/table name from the lab remote cfg
        to identify the table used in influxdb
        """
        monitoring = self.labRemoteCfg["monitoring"]
        return monitoring["measurementName"]

    def alert(self, level, reason):
        """Alert about problems."""
        fromAddr = 'noreply@'+socket.gethostname()
        recipients = self.notification["email-address"]
        subject = f'{level} from {self.getSetup()} software interlock'
        logger.warning(f"{subject}: {reason}")
        msgText = f'{level} from {self.getSetup()} software interlock'
        msgText += '\n%s\n' % (len(msgText)*'-')
        msgText += f'{time.ctime()}   {reason}'
        msg = MIMEText(msgText)
        msg['From'] = fromAddr
        msg['To'] = recipients
        msg['Subject'] = subject
        smtp = smtplib.SMTP('localhost')
        smtp.sendmail(fromAddr, recipients.split(), msg.as_string())
        smtp.quit()

    def lockdown(self):
        """Locks down indicator file until program is manually exited"""
        logger.info("Lockdown in effect")

        logger.info("Locking in progress")
        lock_file_fd = None
        logger.info("Waiting for lock")
        try:
            fcntl.flock(self.interlockFile, fcntl.LOCK_UN)
            logger.info("Lock is enabled")
        except OSError:
            return None
        else:
            lock_file_fd = self.interlockFile
            return lock_file_fd

    def resetIntlck(self):
        """Lifts the lockdown on interlock file."""
        logger.info("Lockdown in effect. Preparing to lift lockdown.")
        lock_file_fd = None
        logger.info("Waiting for lock")
        try:
            fcntl.flock(self.interlockFile, fcntl.LOCK_EX)
            logger.info("Lock is lifted")
        except OSError:
            return None
        else:
            lock_file_fd = self.interlockFile
            return lock_file_fd

    def run(self):
        #quantityFlag = 0 # To keep track of status of all quantities for the case that interlock needs to reset after sensor reading failure

        for quant in self.quantities:
            logger.debug(f"Checking {quant.name}")
            newstatus = quant.checkMeasurement(5) # Updates status of that quantity, sends warnings if needed, returns -1, 1, 2, or 3
            # checkMeasurements(5) returns -1 when either:
            # 1. The query to retrieve the last 5 measurement didn't return any value (most likely loss of communication with influxDB)
            # 2. The last measurement in the influxDB was taken more than 5s (measure-time) ago

            # Case 1: quantity reaches CRITICAL state OR failed to get sensor reading and interlock wasn't already triggered, lock down system
            if (newstatus==3 and self.status!=3) or (newstatus==-1 and self.status!=3):
                quant.status = newstatus # update quantity status
                self.status = newstatus # update overall swint status

                # Lockdown system
                if newstatus==3:
                    self.alert("LOCKDOWN", f"Software interlock triggered by {quant.name} entering CRITICAL state. Measurement = {quant.lastMeas}, shut-down bounds = [{quant.lowerLock}, {quant.upperLock}]. Shutting down system")
                else:
                    self.alert("LOCKDOWN", f"Failed to get sensor readingf for {quant.name}. Shutting down system")
                fd = self.lockdown()
                while fd is None:
                    fd = self.lockdown()

                # Change heartbeat status to 3 (interlock triggered)
                self.heartbeat.writeStatus(self.status)

                # Shut down system
                self.shutdown(quant, quant.quantType)

                logger.info("Interlock lockdown is in effect. Please inspect the NTC for any physical issues")

            # Case 2: quantity reaches WARNING state from GOOD state (interlock hasn't been triggered), send alert
            elif (newstatus==2 and quant.status == 1):
                quant.status = newstatus
                self.status = newstatus # Make sure to track the interlock status as 2 (not triggered)

                # Send email that this quantity is in WARNING state
                self.alert("WARNING", f"{quant.name} has entered WARNING state. Measurement = {quant.lastMeas}, warning bounds = [{quant.lowerWarn}, {quant.upperWarn}]")

            # Case 3: quantity reaches WARNING state from CRITICAL state (interlock has been triggered), lift lockdown
            elif (newstatus==2 and quant.status == 3):
                quant.status = newstatus
                self.status = newstatus # Make sure to track the interlock status as 2 (not triggered)

                # Lift the interlock, send email that this quantity is in warning state
                self.alert("WARNING", f"{quant.name} has entered WARNING state from CRITICAL state. Measurement = {quant.lastMeas}, warning bounds = [{quant.lowerWarn}, {quant.upperWarn}]")
                fd = self.resetIntlck()
                while fd is None:
                    fd = self.resetIntlck()

                logger.info("Interlock has been lifted.")

            # Case 4: quantity becomes GOOD after it was in CRITICAL state (interlock has been triggered), lift lockdown
            elif (newstatus==1 and quant.status == 3):
                quant.status = newstatus
                self.status = newstatus # Make sure to track the interlock status as 1 (not triggered)

                # Lift the interlock
                self.alert("ALLCLEAR", f"{quant.name} has entered GOOD state from CRITICAL state. Measurement = {quant.lastMeas}, warning bounds = [{quant.lowerWarn}, {quant.upperWarn}]")
                fd = self.resetIntlck()
                while fd is None:
                    fd = self.resetIntlck()

                logger.info("Interlock has been lifted.")

            # Case 5: quantity becomes GOOD after it was in WARNING state (interlock has not been triggered)
            elif (newstatus==1 and quant.status == 2):
                quant.status = newstatus
                self.status = newstatus # Make sure to track the interlock status as 1 (not triggered)

                self.alert("ALLCLEAR", f"{quant.name} has entered GOOD state from WARNING state. Measurement = {quant.lastMeas}, warning bounds = [{quant.lowerWarn}, {quant.upperWarn}]")
                logger.info(f"{quant.name} has entered GOOD state from WARNING state. Measurement = {quant.lastMeas}, warning bounds = [{quant.lowerWarn}, {quant.upperWarn}]")

            # Case 6: static GOOD status, do nothing
            elif (newstatus==1 and self.status == 1):
                logger.debug(f"{quant.name} is still in GOOD state. Measurement = {quant.lastMeas}, warning bounds = [{quant.lowerWarn}, {quant.upperWarn}]")

            # Case 7: static WARNING status, do nothing
            elif (newstatus==2 and self.status == 2):
                logger.debug(f"{quant.name} is still in WARNING state. Measurement = {quant.lastMeas}, warning bounds = [{quant.lowerWarn}, {quant.upperWarn}]")

            # Case 8: static CRITICAL status, do nothing
            elif (newstatus==3 and self.status == 3):
                logger.debug(f"{quant.name} is still in CRITICAL state. Measurement = {quant.lastMeas}, shut-down bounds = [{quant.lowerLock}, {quant.upperLock}]")

            # Catch cases outside the above (for improvement)
            else:
                logger.debug("Quantity/interlock status has not changed.")
                pass

    # Ping all lab equipment before starting so we know configuration is correct
    # Also collect list of NTCs corresponding to each PS
    def ping_equip(self):
        cfg = self.makeLabRemoteCfg()
        equipConf = labRemote.ec.EquipConf(cfg)
        logger.info("Checking communication with all devices")
        try:
            for m in self.labRemoteCfg['control']['moduleSetups']:
                logger.debug(f"Checking communication with LV-PS for {m.get('name')}:")
                tmpLV = equipConf.getPowerSupplyChannel(m.get('moduleLV'))
                self.moduleLV += [tmpLV]
                tmpLV.getName()
                logger.debug(f"Checking communication with peltier-PS for {m.get('name')}:")
                tmpPS = equipConf.getPowerSupplyChannel(m.get('peltierPower'))
                tmpPS.getName()
                self.peltierPS += [tmpPS]
                logger.debug(f"Checking communication with HV-PS for {m.get('name')}:")
                if m.get('moduleHV'):
                    tmpHV = equipConf.getPowerSupplyChannel(m.get('moduleHV'))
                    tmpHV.getName()
                    self.moduleHV += [tmpHV]

                # Store module NTC information for query
                ntcName = m.get('moduleNTC')
                ntcQuantity = None
                for device in self.labRemoteCfg['monitorsensors']:
                    if device.get('name') == ntcName:
                        ntcQuantity = device.get('quantity')
                        break
                if ntcQuantity is None:
                    logger.error(f"No quantity found for NTC device associated with {m.get('name')} - please check")
                    exit(0)
                self.moduleNTCs += [ntcQuantity]

            logger.debug("Checking communication with chiller:")
            chillerName = self.labRemoteCfg['control'].get('chiller')
            if chillerName is not None:
                self.chiller = equipConf.getChiller(self.labRemoteCfg['control']['chiller'])
                self.chiller.getStatus()
            else:
                logger.debug("No listed chiller in LabRemote config file, proceeding without chiller control")
        except Exception as e:
            logger.error("Unable to communication with all devices")
            logger.error(e)
            sys.exit(-1)


    ##### Functions to take action ######

    def shutdown(self, quant, quantType):
        if (quantType == "ntc"):
            self.action_high_ntc()
        elif (quantType == "dew-chuck"):
            self.action_dewpoint(quant)
        elif (quantType == "air-flow-rate"):
            self.action_flow()
        elif (quantType == "pressure"):
            #TODO: NEED PRESSURE SENSOR HERE
            logger.error("Pressure placeholder")
        elif (quantType == "nodata"):
            self.action_no_data()
        else:
            logger.warning(f"Quantity type {quantType} not recognized - shutting down system in generic way")
            self.action_generic()

    def action_high_ntc(self):
        logger.warning("Module NTC too high! Turning off LV, HV and peltier")
        for LV in self.moduleLV:
            LV.turnOff()
        for peltier in self.peltierPS:
            peltier.turnOff()
        if self.chiller is not None:
            self.chiller.turnOff()
        # Turn off HV, Luke's edit: put this after LV and peltier since HV needs time to ramp down & to respond
        for HV in self.moduleHV:
            HV.turnOff()
        return 0

    def action_dewpoint(self, quant):
        logger.warning("Dewpoint is within 5C of vacuum chuck temperature! Turning off system.")
        for peltier in self.peltierPS:
            peltier.turnOff()
        if self.chiller is not None:
            self.chiller.turnOff()
        for HV in self.moduleHV:
            HV.turnOff()

        # Added logic for ModuleLV, wait until Module is at 20 deg before shutting LV off
        if self.moduleLV[0].isOn():
            logger.warning("Waiting for Module NTCs to warm up to 20C before turning off LV...")
            allclear = [False for f in self.moduleLV]
            while not all(allclear):
                for i, (ntc, lv) in enumerate(zip(self.moduleNTCs, self.moduleLV)): # Loops through multiple modules in setup
                    if self.module_NTC(ntc, quant) > 20:
                        logger.warning(f"Turning off LV for {ntc}")
                        allclear[i] = True
                        lv.turnOff()
                time.sleep(3)

                # Update heartbeat within the loop so wd can see SWint process
                self.heartbeat.writeStatus(self.status)
                self.heartbeat.beat()
                logger.debug(f"Heartbeat issued during action_dewpoint() loop: {time.ctime()}")

                if not all(allclear):
                    logger.warning("Timeout reached. Not all Module NTCs warmed up to 20C.")
        else:
            logger.info("ModuleLV is already OFF, skipping warm-up check.")
        return 0

    def action_hot_coldplate(self):
        logger.warning("Coldplate is too hot. Turning off LV, HV, peltier and chiller")
        for LV in self.moduleLV:
            LV.turnOff()
        for peltier in self.peltierPS:
            peltier.turnOff()
        if self.chiller is not None:
            self.chiller.turnOff()
        for HV in self.moduleHV:
            HV.turnOff()
        return 0

    def action_generic(self):
        logger.warning("Software interlock triggered but reason unknown. Turning off LV, HV, peltier and chiller")
        for LV in self.moduleLV:
            LV.turnOff()
        for peltier in self.peltierPS:
            peltier.turnOff()
        if self.chiller is not None:
            self.chiller.turnOff()
        for HV in self.moduleHV:
            HV.turnOff()

    def action_hw_interlock(self):
        logger.warning("HW interlock has been triggered. Turning off LV, HV, peltier and chiller")
        for LV in self.moduleLV:
            LV.turnOff()
        for peltier in self.peltierPS:
            peltier.turnOff()
        if self.chiller is not None:
            self.chiller.turnOff()
        for HV in self.moduleHV:
            HV.turnOff()

    def action_flow(self):
        logger.warning("Dry air flow is too low. Shutting down system.")
        for LV in self.moduleLV:
            LV.turnOff()
        for peltier in self.peltierPS:
            peltier.turnOff()
        if self.chiller is not None:
            self.chiller.turnOff()
        for HV in self.moduleHV:
            HV.turnOff()
        return 0

    def action_no_data(self):
        logger.warning("No data queried for quantity. Shutting down system.")
        for LV in self.moduleLV:
            LV.turnOff()
        for peltier in self.peltierPS:
            peltier.turnOff()
        if self.chiller is not None:
            self.chiller.turnOff()
        for HV in self.moduleHV:
            HV.turnOff()
        return 0

    # Function to monitor module NTC (used when dewpoint is too close to vacuum chuck temp)
    # TODO Make this not hard-coded
    def module_NTC(self, ntc, quant, numOfMeas=1):
        query = f'SELECT T FROM "{self.measurementName}" WHERE ("Setup" = \'{self.setupname}\' AND "Quantity" = \'{ntc}\') ORDER BY time DESC LIMIT {numOfMeas}'
        logger.debug(f'Executing query: {query}')
        result = quant.client.query(query)
        logger.debug(f'Query result: {result}')

        tntcList = [float(record.get("T")) for record in result.get_points()]
        logger.debug(f'Extracted temperatures: {tntcList}')

        if not tntcList:
            logger.warning(f'No temperature data found for NTC: {ntc}')
            return float('nan')  # Return NaN if no data is found

        average_temp = sum(tntcList) / numOfMeas
        logger.debug(f'Average temperature for NTC {ntc}: {average_temp}')
        return average_temp

# Function for graceful exit on termination signals, releases heartbeat lock
def graceful_exit(signal_number, frame):
    logger.info(f"Received signal {signal_number}. Shutting down gracefully...")
    logger.debug(f"Signal frame: {frame}")
    swint.heartbeat.lock.release()
    sys.exit(0)

#******************************************
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Command line script for controlling the pixel cooling unit')
    parser.add_argument('-c', '--config', default="cfg.json", help='interlock config file')
    parser.add_argument('--debug', action='store_true', default=False, help='debug output')
    options = parser.parse_args()

    # If in debug mode, go to DEBUG level logging, else set default level to INFO
    if options.debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    #Get settings from cfg file
    logger.info(f"Using software interlock config file: {options.config}")

    #Set up the configuration
    swint = SoftwareInterlock(str(options.config))

    # Set up the quantities to monitor
    quantityList = []
    # TODO - think if this is the best way - what if you forget to enable?
    for data in swint.quantityConfig:
        if not data.get('enabled'):
            logger.debug(f"{data.get('name')} is not enabled - will not monitor")
            continue

        # Define quantity
        quant = Quantity(swint, data)

        #Save it into the sensor list
        quantityList.append(quant)

    swint.quantities = quantityList

    swint.ping_equip()

    signal.signal(signal.SIGINT, graceful_exit)
    signal.signal(signal.SIGTERM, graceful_exit)

    #Analyze measurements
    logger.info("Monitoring quantities")

    # Send initial heartbeat
    swint.heartbeat.writeStatus(swint.status) # Record status of swint, initialize to 1 "GOOD state (not triggered)"
    swint.heartbeat.beat() # Issue heartbeat
    (pid, last_beat) = swint.heartbeat.status()
    logger.debug(f"Heartbeat issued: {pid}, Last Beat={time.ctime(last_beat)}")
    logger.debug("Heartbeat status: " + str(swint.heartbeat.readData()["status"]))

    # Main loop, check quantities forever
    while 1:
        logger.debug("Entering run loop. Will try to run swint.")
        try:
            swint.run() # Run software interlock (main process)

            swint.heartbeat.writeStatus(swint.status) # Make sure to get updated status for the heartbeat"
            swint.heartbeat.beat() # Then issue post-run heartbeat to confirm interlock is still running
            (pid, last_beat) = swint.heartbeat.status()
            logger.debug(f"Heartbeat issued: {pid}, Last Beat={time.ctime(last_beat)}")
            logger.debug("Heartbeat status: " + str(swint.heartbeat.readData()["status"]))
        except Exception as e:
            logger.error(e)
            swint.lockdown()
            swint.shutdown(swint.quantities[0], "nodata")
            exit(0)
        time.sleep(2)
